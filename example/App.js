/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {NativeModules} from 'react-native';

const {SimpleNativeModules} = NativeModules;

const json65mb = require('./data_test/65mb.json');
const json40mb = require('./data_test/40mb.json');
const json15mb = require('./data_test/15mb.json');
const json10mb = require('./data_test/10mb.json');
const x = 999999999;
const y = 999999999;

export default function App() {
  const [result, setResult] = useState('---');
  const [isEnabledLargeData, setIsEnabledLargeData] = useState(false);
  const toggleSwitch = () =>
    setIsEnabledLargeData(previousState => !previousState);

  const getData = () => {
    const jsonString = JSON.stringify(isEnabledLargeData ? json65mb : json15mb);
    console.time('load data by native module');
    // native module
    SimpleNativeModules.getData(jsonString, newData => {
      console.timeEnd('load data by native module');
      // const strLength = newData.length;
      setResult('Load data successful');
    });
  };

  const getDataJSI = () => {
    const jsonString = JSON.stringify(isEnabledLargeData ? json65mb : json15mb);
    // jsi module
    console.time('load data by jsi module');
    const newData = global.getBigData(jsonString);
    console.timeEnd('load data by jsi module');
    // const strLength = newData.length;
    setResult('Load data by jsi successful');
  };

  const multiply = async () => {
    console.time('multiply by native module');
    const res = await SimpleNativeModules.multiply(x, y);
    console.log(res);
    console.timeEnd('multiply by native module');
    setResult(`Multiply successful: ${res}`);
  };

  const multiplyJSI = () => {
    console.time('multiply by jsi module');
    const res = global.multiply(x, y);
    console.log(res);
    console.timeEnd('multiply by jsi module');
    setResult(`Multiply successful: ${res}`);
  };

  const encodeBase64 = async () => {
    // native module
    const jsonString = JSON.stringify(isEnabledLargeData ? json65mb : json15mb);
    console.time('encodeBase64 by native module');
    const res = await SimpleNativeModules.base64FromArrayBuffer(jsonString);
    // console.log(res);
    console.timeEnd('encodeBase64 by native module');
    setResult('Encode data successful');
  };

  const encodeBase64JSI = () => {
    // jsi module
    const jsonString = JSON.stringify(isEnabledLargeData ? json65mb : json15mb);
    console.time('encodeBase64 by jsi module');
    const res = global.base64FromArrayBuffer(jsonString);
    // console.log(res);
    console.timeEnd('encodeBase64 by jsi module');
    setResult('Encode data successful');
  };

  return (
    <SafeAreaView>
      <ScrollView contentInsetAdjustmentBehavior="automatic">
        <View style={styles.switchRow}>
          <View>
            <Switch
              trackColor={{false: '#767577', true: '#81b0ff'}}
              thumbColor={isEnabledLargeData ? '#f5dd4b' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabledLargeData}
            />
          </View>
          <View>
            <Text
              style={{
                fontSize: 20,
                color: `${isEnabledLargeData ? '#f5dd4b' : 'gray'}`,
              }}>
              Large data
            </Text>
          </View>
        </View>

        <View style={styles.actionButton}>
          <TouchableOpacity onPress={getData}>
            <Text>Get string by native module</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{paddingVertical: 10}} onPress={getDataJSI}>
            <Text>Get string by JSI</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={multiply}>
            <Text>Multiply by native module</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{paddingVertical: 10}} onPress={multiplyJSI}>
            <Text>Multiply by JSI</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={encodeBase64}>
            <Text>Encode Base64 by native module</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{paddingVertical: 10}}
            onPress={encodeBase64JSI}>
            <Text>Encode Base64 by JSI</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.switchRow}>
          <Text style={{fontSize: 18}}>{result}</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 24,
  },
  actionButton: {
    padding: 20,
  },
  switchRow: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
});
