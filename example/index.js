import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-console-time-polyfill';

import MessageQueue from 'react-native/Libraries/BatchedBridge/MessageQueue.js';

const spyFunction = msg => {
  // native to javascript
  if (msg?.type === 0) {
    console.log('Bridge method: ', msg?.method);
  }
};

MessageQueue.spy(spyFunction);

AppRegistry.registerComponent(appName, () => App);
