package com.reactnativesimplejsi;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.JavaScriptContextHolder;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.module.annotations.ReactModule;

import java.util.Base64;

@ReactModule(name = SimpleJsiModule.NAME)
public class SimpleJsiModule extends ReactContextBaseJavaModule {
    public static final String NAME = "SimpleNativeModules";

    public SimpleJsiModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    @NonNull
    public String getName() {
        return NAME;
    }

    static {
        try {
            // Used to load the 'native-lib' library on application startup.
            System.loadLibrary("reactnativesimplejsi");
        } catch (Exception ignored) {
        }
    }

    private native void nativeInstall(long jsi);
    public void installLib(JavaScriptContextHolder reactContext) {

      if (reactContext.get() != 0) {
        this.nativeInstall(
          reactContext.get()
        );
      } else {
        Log.e("SimpleJsiModule", "JSI Runtime is not available in debug mode");
      }

    }

//    public static native int nativeMultiply(int a, int b);
//    @ReactMethod
//    public void multiply(int a, int b, Promise promise) {
//      promise.resolve(nativeMultiply(a, b));
//    }

    @ReactMethod
    public void multiply(int a, int b, Promise promise) {
      promise.resolve(a*b);
    }

    @ReactMethod
    public void getData(String input, Callback cb) {
      StringBuilder builder = new StringBuilder(input);
      builder.append("\nadd them ti ne!");
      cb.invoke(builder.toString());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @ReactMethod
    public void base64FromArrayBuffer(String input, Promise promise) {
      String encodedString = Base64.getEncoder().encodeToString(input.getBytes());
      StringBuilder builder = new StringBuilder(encodedString);
      promise.resolve(builder.toString());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @ReactMethod
    public void base64ToArrayBuffer(String input, Promise promise) {
      String decodedString =  Base64.getEncoder().encodeToString(input.getBytes());
      StringBuilder builder = new StringBuilder(decodedString);
      promise.resolve(builder.toString());
    }

}
