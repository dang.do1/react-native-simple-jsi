#include <jni.h>
#include "react-native-simple-jsi.h"

//extern "C"
//JNIEXPORT jint JNICALL
//Java_com_reactnativesimplejsi_SimpleJsiModule_nativeMultiply(JNIEnv *env, jclass type, jint a, jint b) {
//    return example::multiply(a, b);
//}

extern "C"
JNIEXPORT void JNICALL
Java_com_reactnativesimplejsi_SimpleJsiModule_nativeInstall(JNIEnv *env, jobject thiz, jlong jsi) {
    // TODO: implement nativeInstall()
    auto runtime = reinterpret_cast<facebook::jsi::Runtime *>(jsi);

    if (runtime) {
        example::install(*runtime);
    }
}
