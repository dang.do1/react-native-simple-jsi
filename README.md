# react-native-simple-jsi

build a simple jsi

## Installation

```sh
npm install react-native-simple-jsi
```

## Usage

```js
import { multiply } from "react-native-simple-jsi";

// ...

const result = await multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
