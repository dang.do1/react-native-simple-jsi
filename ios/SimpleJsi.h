#import <React/RCTBridgeModule.h>

// #ifdef __cplusplus

// #import "react-native-simple-jsi.h"

// #endif

@interface SimpleNativeModules : NSObject <RCTBridgeModule>

@property(nonatomic, assign) BOOL setBridgeOnMainQueue;

@end
