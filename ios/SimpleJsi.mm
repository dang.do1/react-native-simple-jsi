#import "SimpleJsi.h"
#import "react-native-simple-jsi.h"
#import <React/RCTBridge+Private.h>
#import <React/RCTUtils.h>
#import <jsi/jsi.h>
#import <CommonCrypto/CommonCrypto.h>
#import <Foundation/Foundation.h>

@implementation SimpleNativeModules


@synthesize bridge = _bridge;
@synthesize methodQueue = _methodQueue;


RCT_EXPORT_MODULE();


+ (BOOL)requiresMainQueueSetup {
  return YES;
}

- (void)setBridge:(RCTBridge *)bridge
{
  _bridge = bridge;
  _setBridgeOnMainQueue = RCTIsMainQueue();
  
  [self setup];
}

- (void)setup {
  RCTCxxBridge *cxxBridge = (RCTCxxBridge *)self.bridge;
  if (!cxxBridge.runtime) {
    // retry 10ms later - THIS IS A WACK WORKAROUND. wait for TurboModules to land.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
      [self setup];
    });
    return;
  }
  
    example::install(*(facebook::jsi::Runtime *)cxxBridge.runtime);
}

- (void)invalidate {
    example::cleanupJSI();
}


RCT_EXPORT_METHOD(multiply:(nonnull NSNumber*)a withB:(nonnull NSNumber*)b
                  withResolver:(RCTPromiseResolveBlock)resolve
                  withReject:(RCTPromiseRejectBlock)reject)
{
    NSNumber *result = @([a floatValue] * [b floatValue]);

    resolve(result);
}

RCT_EXPORT_METHOD(getData: (NSString*)input callback:(RCTResponseSenderBlock)callback) {
  NSString* newString = [NSString stringWithFormat:@"%@%@", input, @"\nadd them ne!"];
  callback(@[newString]);
}

RCT_EXPORT_METHOD(base64FromArrayBuffer: (NSString*)input 
                  withResolver:(RCTPromiseResolveBlock)resolve
                  withReject:(RCTPromiseRejectBlock)reject) {
  NSData* nsdata = [input dataUsingEncoding:NSUTF8StringEncoding];
  NSString* base64Encoded = [nsdata base64EncodedStringWithOptions:0];
  resolve(base64Encoded);
}

@end