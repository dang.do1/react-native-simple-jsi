#include "react-native-simple-jsi.h"
#include "base64.h"
#include "base64.cpp"
#include <jsi/jsi.h>

using namespace facebook::jsi;
using namespace std;
#include <iostream>
#include <sstream>
#include <string>

// Returns false if the passed value is not a string or an ArrayBuffer.
bool valueToString(Runtime &runtime, const Value &value, std::string *str)
{
	if (value.isString())
	{
		*str = value.asString(runtime).utf8(runtime);
		return true;
	}

	if (value.isObject())
	{
		auto obj = value.asObject(runtime);
		if (!obj.isArrayBuffer(runtime))
		{
			return false;
		}
		auto buf = obj.getArrayBuffer(runtime);
		*str = std::string((char *)buf.data(runtime), buf.size(runtime));
		return true;
	}

	return false;
}

namespace example
{
	void cleanupJSI()
	{
		std::cout << "Cleanup JSIModule"
							<< "\n";
	}

	void install(Runtime &jsiRuntime)
	{
		std::cout << "Initializing react-native-simple-jsi"
							<< "\n";

		auto helloWorld = Function::createFromHostFunction(jsiRuntime,
																											 PropNameID::forAscii(jsiRuntime,
																																						"helloWorld"),
																											 0,
																											 [](Runtime &runtime,
																													const Value &thisValue,
																													const Value *arguments,
																													size_t count) -> Value
																											 {
																												 string helloworld = "helloworld";

																												 return Value(runtime,
																																			String::createFromUtf8(
																																					runtime,
																																					helloworld));
																											 });

		jsiRuntime.global().setProperty(jsiRuntime, "helloWorld", move(helloWorld));

		auto multiply = Function::createFromHostFunction(jsiRuntime,
																										 PropNameID::forAscii(jsiRuntime,
																																					"multiply"),
																										 2,
																										 [](Runtime &runtime,
																												const Value &thisValue,
																												const Value *arguments,
																												size_t count) -> Value
																										 {
																											 int x = arguments[0].getNumber();
																											 int y = arguments[1].getNumber();

																											 return Value(x * y);
																										 });

		jsiRuntime.global().setProperty(jsiRuntime, "multiply", move(multiply));

		auto multiplyWithCallback = Function::createFromHostFunction(jsiRuntime,
																																 PropNameID::forAscii(jsiRuntime,
																																											"multiplyWithCallback"),
																																 3,
																																 [](Runtime &runtime,
																																		const Value &thisValue,
																																		const Value *arguments,
																																		size_t count) -> Value
																																 {
																																	 int x = arguments[0].getNumber();
																																	 int y = arguments[1].getNumber();

																																	 arguments[2].getObject(runtime).getFunction(runtime).call(runtime, x * y);

																																	 return Value();
																																 });

		jsiRuntime.global().setProperty(jsiRuntime, "multiplyWithCallback", move(multiplyWithCallback));

		auto base64FromArrayBuffer = Function::createFromHostFunction(
				jsiRuntime,
				PropNameID::forAscii(jsiRuntime, "base64FromArrayBuffer"),
				1, // string
				[](Runtime &runtime, const Value &thisValue, const Value *arguments, size_t count) -> Value
				{
					std::string str;
					if (!valueToString(runtime, arguments[0], &str))
					{
						return Value(-1);
					}
					std::string strBase64 = base64_encode(str);

					return Value(String::createFromUtf8(runtime, strBase64));
				});
		jsiRuntime.global().setProperty(jsiRuntime, "base64FromArrayBuffer", std::move(base64FromArrayBuffer));

		auto base64ToArrayBuffer = Function::createFromHostFunction(
				jsiRuntime,
				PropNameID::forAscii(jsiRuntime, "base64ToArrayBuffer"),
				1, // string
				[](Runtime &runtime, const Value &thisValue, const Value *arguments, size_t count) -> Value
				{
					if (!arguments[0].isString())
					{
						return Value(-1);
					}

					std::string strBase64 = arguments[0].getString(runtime).utf8(runtime);
					std::string str = base64_decode(strBase64);

					Function arrayBufferCtor = runtime.global().getPropertyAsFunction(runtime, "ArrayBuffer");
					Object o = arrayBufferCtor.callAsConstructor(runtime, (int)str.length()).getObject(runtime);
					ArrayBuffer buf = o.getArrayBuffer(runtime);
					memcpy(buf.data(runtime), str.c_str(), str.size());

					return o;
				});
		jsiRuntime.global().setProperty(jsiRuntime, "base64ToArrayBuffer", std::move(base64ToArrayBuffer));

		auto getBigData = Function::createFromHostFunction(
				jsiRuntime,
				PropNameID::forUtf8(jsiRuntime, "getBigData"),
				2,
				[](Runtime &runtime, const Value &thisValue, const Value *arguments, size_t count) -> Value
				{
					std::string string = arguments[0].asString(runtime).utf8(runtime);

					string.append("\nadd them ti ne!");

					return String::createFromUtf8(runtime, string);
				});
		jsiRuntime.global().setProperty(jsiRuntime, "getBigData", std::move(getBigData));
	}
}
