#ifndef EXAMPLE_H
#define EXAMPLE_H

namespace facebook
{
  namespace jsi
  {
    class Runtime;
  }
}

namespace example
{
  void install(facebook::jsi::Runtime &jsiRuntime);
  void cleanupJSI();
}

#endif /* EXAMPLE_H */
